#include "grader.h"

#include <cstdlib>
#include <cassert>

void DropCaches()
{
#ifdef __APPLE__
    assert(system("sudo purge") == 0);
#else
    assert(system("sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'") == 0);
#endif
}

